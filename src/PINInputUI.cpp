/**

   PINInputUI.cpp implementation of PINInputUI.h

   PIN Input UI to used with u8g2lib.h

   Copyright 2023 Ragil Rynaldo Meyer <meyer.ananda@gmail.com>

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

*/

#include "PINInputUI.h"

void PinInputUI::loop() {
  if (!this->mShown)
    return;

  uint8_t displayWidth = this->pDisplay->getDisplayWidth();

  // uint8_t displayHeight = this->pDisplay->getDisplayHeight();

  this->pDisplay->setFont(
      PIN_INPUT_UI_ALPHABET_FONT); // width = 13, height = 20
  uint8_t maxCharHeight = this->pDisplay->getMaxCharHeight();
  uint8_t maxCharWidth = this->pDisplay->getMaxCharWidth();

  this->pDisplay->setFont(PIN_INPUT_UI_ICON_FONT);
  uint8_t maxIconHeight = this->pDisplay->getMaxCharHeight();
  uint8_t maxIconWidth = this->pDisplay->getMaxCharWidth();

  uint8_t yTopPos = this->mStartYPos + 4 + maxIconHeight;

  uint8_t pinBoxWidth =
      (displayWidth / PIN_INPUT_UI_PIN_BOX_TOTAL) - PIN_INPUT_UI_PIN_BOX_MARGIN;
  uint8_t pinBoxHeight = maxCharHeight + 8;

  for (uint8_t i = 0; i < PIN_INPUT_UI_PIN_BOX_TOTAL; i++) {
    uint8_t boxXPos = (pinBoxWidth * i) + (PIN_INPUT_UI_PIN_BOX_MARGIN * i);

    this->mActivePinBox == i
        ? this->pDisplay->drawBox(boxXPos, yTopPos, pinBoxWidth, pinBoxHeight)
        : this->pDisplay->drawFrame(boxXPos, yTopPos, pinBoxWidth,
                                    pinBoxHeight);

    this->pDisplay->setFont(PIN_INPUT_UI_ALPHABET_FONT);
    this->pDisplay->setCursor(boxXPos + ((pinBoxWidth - maxCharWidth) / 2),
                              yTopPos + maxCharHeight);

    this->mActivePinBox == i
        ? this->pDisplay->print(this->mVals[this->mActivePinBox])
        : this->pDisplay->print(F("*"));

    if (this->mActivePinBox == i && this->mPinBoxEditable) {
      this->pDisplay->setFont(PIN_INPUT_UI_ICON_FONT);
      this->pDisplay->drawGlyph(boxXPos + ((pinBoxWidth - maxIconWidth) / 2),
                                yTopPos - 2,
                                PIN_INPUT_UI_BOLD_HEAD_ARROW_UP_ICON);
      this->pDisplay->drawGlyph(boxXPos + ((pinBoxWidth - maxIconWidth) / 2),
                                yTopPos + pinBoxHeight + maxIconHeight + 2,
                                PIN_INPUT_UI_BOLD_HEAD_ARROW_DOWN_ICON);
    }
  }
}

uint8_t PinInputUI::getActiveBox() {
  if (!this->mShown)
    return 0;
  return this->mActivePinBox;
}

void PinInputUI::inc() {
  if (this->mPinBoxEditable) {
    if (this->mVals[this->mActivePinBox] == PIN_INPUT_UI_PIN_MAX_NUMBER_VALUE)
      return;
    this->mVals[this->mActivePinBox] += 1;
    return;
  }

  if (this->mActivePinBox == PIN_INPUT_UI_PIN_BOX_TOTAL - 1)
    return;
  this->mActivePinBox += 1;
}

void PinInputUI::dec() {
  if (this->mPinBoxEditable) {
    if (this->mVals[this->mActivePinBox] == PIN_INPUT_UI_PIN_MIN_NUMBER_VALUE)
      return;
    this->mVals[this->mActivePinBox] -= 1;
    return;
  }

  if (this->mActivePinBox == 0)
    return;
  this->mActivePinBox -= 1;
}

uint8_t PinInputUI::getActiveValue() {
  if (!this->mShown)
    return 0;

  return this->mVals[this->mActivePinBox];
}

uint8_t *PinInputUI::getValues() { return this->mVals; }

void PinInputUI::toggleEdit() {
  if (!this->mShown)
    return;
  this->mPinBoxEditable = !this->mPinBoxEditable;
}

bool PinInputUI::isEditable() {
  if (!this->mShown)
    return false;
  return this->mPinBoxEditable;
}

void PinInputUI::hide() { this->mShown = false; }

void PinInputUI::show() { this->mShown = true; }

void PinInputUI::setOnCommit(pinCommitHandler_t handler) {
  this->mOnCommitHandler = handler;
}

void PinInputUI::commit() {
  if (!this->mShown || !this->mOnCommitHandler)
    return;
  this->mOnCommitHandler(this->mVals);
}

bool PinInputUI::isShown() { return this->mShown; }

void PinInputUI::clear() {
  for (uint8_t i = 0; i < PIN_INPUT_UI_PIN_BOX_TOTAL; i++) {
    this->mVals[i] = 0;
  }
  this->mActivePinBox = 0;
  this->mPinBoxEditable = 0;
}

void PinInputUI::reset() {
  this->clear();
  this->mOnCommitHandler = NULL;
  this->mShown = false;
}
