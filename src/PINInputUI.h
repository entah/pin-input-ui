/**

   PINInputUI.h

   PIN Input UI to used with u8g2lib.h

   Copyright 2023 Ragil Rynaldo Meyer <meyer.ananda@gmail.com>

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

*/

#ifndef PIN_INPUT_UI_H
#define PIN_INPUT_UI_H

#pragma once

#include <Arduino.h>
#include <U8g2lib.h>
#include <functional>

#define PIN_INPUT_UI_PIN_MAX_NUMBER_VALUE 9
#define PIN_INPUT_UI_PIN_MIN_NUMBER_VALUE 0
#define PIN_INPUT_UI_PIN_BOX_TOTAL 4
#define PIN_INPUT_UI_PIN_BOX_MARGIN 2
#define PIN_INPUT_UI_BOLD_HEAD_ARROW_DOWN_ICON 109
#define PIN_INPUT_UI_BOLD_HEAD_ARROW_UP_ICON 112
#define PIN_INPUT_UI_ALPHABET_FONT u8g2_font_crox3cb_mr
#define PIN_INPUT_UI_ICON_FONT u8g2_font_open_iconic_all_1x_t

/**
typedef pinCommitHandler_t

Function to call when commiting inputted pin.
@param `values` - pointer to list of stored pin.
@return void
 */
typedef std::function<void(uint8_t *values)> pinCommitHandler_t;

/**
class PinInputUI

Used to render PIN Input UI.

@param [in] `*display` - pointer to display used.
@param [in] `startYPos` - Y position to start render the UI, default zero to
show full display height.
*/
class PinInputUI {
public:
  explicit PinInputUI(U8G2 *display, uint8_t startYPos = 0) {
    this->mStartYPos = startYPos;
    this->pDisplay = display;
  };

  // Use in loop to update the display.
  void loop();

  // Increase value of PIN (number) when in edit mode, or change to next input
  // box.
  void inc();

  // Decrease value of PIN (number) when in edit mode, or change to prev input
  // box.
  void dec();

  // Hide the UI.
  void hide();

  // Show the UI.
  void show();

  // Clear stored PIN value, also the active/highlighted input box.
  void clear();

  // Reset all values but show/hidden value.
  void reset();

  // Commit stored PIN value.
  void commit();

  // True if not in hidden state.
  bool isShown();

  // True if not in edit mode.
  bool isEditable();

  // Toggel between edit mode.
  void toggleEdit();

  // Get pointer to stored PIN value.
  uint8_t *getValues();

  // Get active/highlighted input box index.
  uint8_t getActiveBox();

  // Get value of active/highlighted input box.
  uint8_t getActiveValue();

  // Set handler to execute upon commited.
  void setOnCommit(pinCommitHandler_t handler);

private:
  // variable to hold stored PIN values.
  uint8_t mVals[4] = {0, 0, 0, 0};
  // variable to indicate active/highlighted input box.
  uint8_t mActivePinBox = 0;
  // variable to indicate active/highlighted input box is in edit mode.
  bool mPinBoxEditable = false;
  // variable to indicate UI should be shown.
  bool mShown = false;
  // variable to hold handler executed upon commiting stored PIN values..
  pinCommitHandler_t mOnCommitHandler = NULL;
  // variable to indicate Y position of display to render.
  uint8_t mStartYPos = 0;
  // variable hold the pointer of display object.
  U8G2 *pDisplay = NULL;
}; // class PinInputUI

#endif

// Local Variables:
// mode: c++
// End:
